/*
 Copyright 2012 University of California, San Diego

 Licensed under the Apache License, Version 1.1 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-1.1

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/**
 * DataGather_Service.java
 * 
 * send broadcast to the other modules to start
 * create the DataGather service
 * Setup the IOIO board 
 * control the IOIO board
 * start/stop the SerialLineController
 * 
 * @author Gesuri Ramirez, Peter Shin
 * @date August 2012
 */

package org.cleos.android.ntl.datagather;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;

import org.cleos.android.lib.Utils;
import org.cleos.android.lib.Write2File;
import org.cleos.android.ntl.broadcasts.SendBroadcast;
import org.cleos.android.ntl.utils.CommandList;
import org.cleos.android.ntl.utils.Configurator;
import org.cleos.android.ntl.utils.IOIOParameters;

import ioio.lib.api.AnalogInput;
import ioio.lib.api.DigitalOutput;
import ioio.lib.api.IOIO;
import ioio.lib.api.IOIOFactory;
import ioio.lib.api.Uart;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.api.exception.IncompatibilityException;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class DataGather_Service extends Service {

	private Context DGServiceContext = this;

	/** Baud rate for the UARTs */
	private int baud = 9600;

	private String TAG = getClass().getSimpleName();
	private int pinRx1, pinRx2, pinRx3, pinTx1, pinTx2, pinTx3;

	private IOIOThread ioio_thread_;

	/** The uart objects from the IOIOboard */
	private Uart uart1;
	private Uart uart2;
	private Uart uart3;
	private OutputStream out1; // write to the serial port 1
	private InputStream in1; // reading the serial port 1
	private OutputStream out2; // write to the serial port 2
	private InputStream in2; // reading the serial port 2
	private OutputStream out3; // write to the serial port 3
	private InputStream in3; // reading the serial port 3
	private AnalogInput temperature; // onBoard temperature sensor
	private AnalogInput humidity; // onBoard humidity sensor
	private AnalogInput voltage; // onBoard voltage sensor

	/** Object to write a log */
	private Write2File log;

	private LinkedList<SerialLineController> slcThreads;

	private boolean isRunningService = false;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		startDT();
		runService();
		return (START_NOT_STICKY);
	}

	private void startDT() {
		// unlock DT service
		SendBroadcast.unlockDTservice(DGServiceContext);
		Utils.wait(10);
		// lock the DLP service
		SendBroadcast.lockDLPService(DGServiceContext);
		// start the DT server and then start DLP
		SendBroadcast.restartRBNB(DGServiceContext);
		// ----- for DLP4RDT
		SendBroadcast.lockDLP4RDTService(DGServiceContext);
		SendBroadcast.restartDLP4RDT(DGServiceContext);
	}

	private void runService() {
		if (!isRunningService) {
			Log.w(getClass().getName(), "Got to runService()!");
			isRunningService = true;

			Notification note = new Notification(R.drawable.ic_launcher,
					"The service is running", System.currentTimeMillis());
			Intent i = new Intent(this, DataGather_Activity.class);

			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);

			PendingIntent pi = PendingIntent.getActivity(this, 0, i, 0);

			note.setLatestEventInfo(this, "IOIO_sonde_templine",
					"Now Running!", pi);
			note.flags |= Notification.FLAG_NO_CLEAR;

			setupRunIOIO();
			msgToast("Services started on runService()");
			log.writelnT("Services started on runService()");
			startForeground(1337, note);
		}
	}

	private void msgToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
		// log.writelnT(msg);
	}

	@Override
	public IBinder onBind(Intent intent) {
		// We don't provide binding, so return null
		return null;
	}

	@Override
	public void onDestroy() {
		stop();
		Toast.makeText(this, "service done onDestroy", Toast.LENGTH_LONG)
				.show();
		log.writelnT("Service donde onDestroy().");
	}

	private void stop() {

		if (isRunningService) {
			log.writelnT("Service stopped from stop().");
			stopThreads();
			Log.w(getClass().getName(), "Got to stop()!");
			isRunningService = false;
			stopForeground(true);
			msgToast("Stoped service on stop()");
		}
	}

	// -----------------------------------------

	private void setupRunIOIO() {
		setUart1(5, 6, baud);
		setUart2(3, 4, baud);
		setUart3(10, 11, baud);
		log = new Write2File(TAG, "log.txt", true); // create the log file
		this.ioio_thread_ = new IOIOThread();
		this.ioio_thread_.start();
	}

	private void stopThreads() {
		killSLCThreads(this.slcThreads);
		if (ioio_thread_ != null)
			ioio_thread_.abort();

		try {
			ioio_thread_.join();
		} catch (InterruptedException e) {
		}
	}

	private void setUart1(int pinRx, int pinTx, int baud) {
		this.pinRx1 = pinRx;
		this.pinTx1 = pinTx;
		this.baud = baud;
	}

	private void setUart2(int pinRx, int pinTx, int baud) {
		this.pinRx2 = pinRx;
		this.pinTx2 = pinTx;
		this.baud = baud;
	}

	private void setUart3(int pinRx, int pinTx, int baud) {
		this.pinRx3 = pinRx;
		this.pinTx3 = pinTx;
		this.baud = baud;
	}

	private void killSLCThreads(LinkedList<SerialLineController> slcList) {
		if (slcList != null) {
			for (SerialLineController slc : slcList) {
				slc.interrupt();
				slc.abort();
				try {
					slc.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		log.writelnT("killSLCThreads() was called.");
	}

	class IOIOThread extends Thread {
		private IOIO ioio_; // Creates the IOIO object
		private boolean abort_ = false; // Flag in case of abort
		DigitalOutput led; // to control onboard LED

		/** Thread body. */
		@Override
		public void run() {
			super.run();
			loopIOIO();
		}// end run()

		public void loopIOIO() {
			while (true) {
				synchronized (this) {
					if (abort_) {
						killSLCThreads(slcThreads);
						break;
					}
					ioio_ = IOIOFactory.create();
				}
				try {
					setupIOIOConnection();
					setupIOIO_IOs();
					log.writelnT("IOIO Connection established.");
					slcThreads = spawnSLCs();
					while (true) {

						if (abort_) {
							killSLCThreads(slcThreads);
							break;
						}

						led.write(false);
						sleep(100);
						led.write(true);
						sleep(100);
					}
				} catch (ConnectionLostException e) {
				} catch (Exception e) {
					killSLCThreads(slcThreads); // kill all the threads
					Log.e("IOIO", "Unexpected exception caught", e);
					log.writelnT("IOIO disconected in unexpected exception: "
							+ e.toString());
					ioio_.disconnect();
					break;
				} finally {
					if (ioio_ != null) {
						log.writelnT("KillSLCThreads on finally ...");
						killSLCThreads(slcThreads);
						try {
							ioio_.waitForDisconnect();
						} catch (InterruptedException e) {
						}
					}
					synchronized (this) {
						ioio_ = null;
					}
				}
			}
		}

		/**
		 * @param the
		 *            instrument information, port configuration, UI handle,
		 *            system information
		 * @return list of handles to the threads
		 * */
		public LinkedList<SerialLineController> spawnSLCs() {
			Configurator conf = new Configurator();
			LinkedList<SerialLineController> slcList = new LinkedList<SerialLineController>();

			// Sonde
			IOIOParameters ioioParametersSonde = new IOIOParameters(in1, out1);
			CommandList cmdSonde = conf.createSondeCmdList("Sonde");
			SerialLineController sonde = new SerialLineController(
					DGServiceContext, cmdSonde, ioioParametersSonde, TAG + "/"
							+ "Sonde");

			// Templine
			IOIOParameters ioioParametersTempline = new IOIOParameters(in2,
					out2);
			CommandList cmdTempline = conf.createTemplineCmdList("Templine");
			SerialLineController templine = new SerialLineController(
					DGServiceContext, cmdTempline, ioioParametersTempline, TAG
							+ "/" + "Templine");

			// onBoardTemp
			IOIOParameters ioioParametersTemp = new IOIOParameters(temperature);
			CommandList cmdTemp = conf.createTempCmdList("temperature");
			SerialLineController tempTemp = new SerialLineController(
					DGServiceContext, cmdTemp, ioioParametersTemp, TAG + "/"
							+ "temperature");

			// onBoardHumi
			IOIOParameters ioioParametersHumi = new IOIOParameters(humidity);
			CommandList cmdHumi = conf.createHumiCmdList("humidity");
			SerialLineController tempHumi = new SerialLineController(
					DGServiceContext, cmdHumi, ioioParametersHumi, TAG + "/"
							+ "humidity");

			// onBoardVolt
			IOIOParameters ioioParametersVolt = new IOIOParameters(voltage);
			CommandList cmdVolt = conf.createVoltCmdList("voltage");
			SerialLineController tempVolt = new SerialLineController(
					DGServiceContext, cmdVolt, ioioParametersVolt, TAG + "/"
							+ "voltage");

			// Vaisela Weather Station
			IOIOParameters ioioParametersVWS = new IOIOParameters(in3, out3);
			CommandList cmdVWS = conf.createVWSCmdList("VWS");
			SerialLineController tempVWS = new SerialLineController(
					DGServiceContext, cmdVWS, ioioParametersVWS, TAG + "/"
							+ "VWS");

			tempTemp.start();
			slcList.add(tempTemp);

			tempHumi.start();
			slcList.add(tempHumi);

			tempVolt.start();
			slcList.add(tempVolt);

			templine.start();
			slcList.add(templine);

			sonde.start();
			slcList.add(sonde);

			tempVWS.start();
			slcList.add(tempVWS);

			return slcList;
		}

		private void setupIOIOConnection() throws ConnectionLostException,
				IncompatibilityException {
			ioio_.waitForConnect();
		}// end setupIOIOConnection()

		private void setupIOIO_IOs() throws ConnectionLostException {
			led = ioio_.openDigitalOutput(0, true);
			uart1 = ioio_.openUart(pinRx1, pinTx1, baud, Uart.Parity.NONE,
					Uart.StopBits.ONE);
			uart2 = ioio_.openUart(pinRx2, pinTx2, baud, Uart.Parity.NONE,
					Uart.StopBits.ONE);
			uart3 = ioio_.openUart(pinRx3, pinTx3, baud, Uart.Parity.NONE,
					Uart.StopBits.ONE);
			// in and out streams
			out1 = uart1.getOutputStream();
			in1 = uart1.getInputStream();
			out2 = uart2.getOutputStream();
			in2 = uart2.getInputStream();
			out3 = uart3.getOutputStream();
			in3 = uart3.getInputStream();
			temperature = ioio_.openAnalogInput(46);
			humidity = ioio_.openAnalogInput(45);
			voltage = ioio_.openAnalogInput(44);
		}// end setupIOIO_IOs()

		/**
		 * Abort the connection.
		 * 
		 * This is a little tricky synchronization-wise: we need to be handle
		 * the case of abortion happening before the IOIO instance is created or
		 * during its creation.
		 */
		synchronized public void abort() {
			abort_ = true;
			if (ioio_ != null) {
				ioio_.disconnect();
			}
		}

	}// end IOIOThread

}
