/*
 Copyright 2012 University of California, San Diego

 Licensed under the Apache License, Version 1.1 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-1.1

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/**
 * DataLineProcessor4RemoteDT_Service.java
 * 
 * Create a service for the DLP4RDT service if there is no one
 * Start the DLP4RDT threads for each sensor
 * Receive the data for each sensor and then look for the appropriate DLP
 *  thread to process the line 
 * 
 * 
 * @author Gesuri Ramirez, Peter Shin
 */

package org.cleos.ntl.datalineprocessor4remotedt;

import java.util.LinkedList;

import org.cleos.android.lib.Constants;
import org.cleos.android.lib.Utils;
import org.cleos.android.lib.Write2File;
import org.cleos.android.ntl.utils.CommandList;
import org.cleos.android.ntl.utils.Configurator;
import org.cleos.ntl.datalineprocessor4remotedt.lib.DTSrcManager;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class DataLineProcessor4RemoteDT_Service extends Service {
	private String TAG = getClass().getSimpleName();
	private boolean isRunningService = false;
	private String flagFile = Constants.LOCK_DLP_FLAG_FILE;
	private Write2File log = new Write2File(
			"DataLineProcessor4RemoteDT/" + TAG, TAG + ".txt", true);
	DTSrcManager dtSrc;
	private final IBinder mBinder = new LocalBinder();
	private LinkedList<DataLineProcessor_x> DLPs;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		runService(intent.getExtras());
		return (START_NOT_STICKY);
	}

	private void runService(Bundle extras) {

		Log.i(TAG, "The service is running? " + isRunningService);
		if (!isRunningService) {
			log.writelnT("The DataLineProcessor4RemoteDT service will start.");
			// Log.i(TAG, "Got to runService()!");
			isRunningService = true;

			Notification note = new Notification(R.drawable.ic_launcher,
					"The DataLineProcessor4RemoteDT service is running",
					System.currentTimeMillis());
			Intent i = new Intent(this,
					DataLineProcessor4RemoteDT_Service.class);

			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);

			PendingIntent pi = PendingIntent.getActivity(this, 0, i, 0);

			note.setLatestEventInfo(this, TAG, "Now Running!", pi);
			note.flags |= Notification.FLAG_NO_CLEAR;

			msgToast("Services started on runService()");
			// log.writelnT("Services started on runService()");
			startForeground(1337, note);
		}

		if (extras != null) {
			String slcName = extras.getString(Constants.SLC_NAME);
			String dataLine = extras.getString(Constants.DATALINE);
			// look for the correct DLP_x
			if (DLPs != null) {
				for (DataLineProcessor_x dlpx : DLPs) {
					if (dlpx.getDLPName().equalsIgnoreCase(slcName)) {
						dlpx.processDataLine(dataLine);
						break;
					}
				}
			} else
				DLPs = spawnDLPs();
		} else {
			Log.i(TAG, "The DataLineProcessor4RemoteDT will restart.");
			log.writelnT("The DataLineProcessor4RemoteDT will restart.");

			// always close and create connection

			// destroy all the DataLineProcessor_x

			destroyAllDLP_x();

			// create all the DataLineProcessor_x

			DLPs = spawnDLPs();

			// after the source and the connection to the RBNB is open the
			// service will be unlock
			Utils.lockService(this, flagFile, false);
		}
	}

	private void destroyAllDLP_x() {
		if (DLPs != null) {
			for (DataLineProcessor_x dlpx : DLPs) {
				dlpx.kill();
				Utils.wait(1000);
			}
			DLPs.clear();
		}

	}

	private LinkedList<DataLineProcessor_x> spawnDLPs() {
		Configurator conf = new Configurator();
		LinkedList<DataLineProcessor_x> dlpList = new LinkedList<DataLineProcessor_x>();

		// Sonde
		DataLineProcessor_x dlpSonde = new DataLineProcessor_x(this, "Sonde");
		CommandList cmdSonde = conf.createSondeCmdList("Sonde");
		dlpSonde.setAddressAndPort(cmdSonde.getRemoteDTAddress()[0]);
		dlpSonde.setChNames(cmdSonde.getChNames()[0]);
		dlpSonde.setDelimiter(cmdSonde.getDelimiter()[0]);
		dlpSonde.setdTypes(cmdSonde.getDTypes()[0]);
		dlpSonde.setMIMEs(cmdSonde.getMIMEs()[0]);
		dlpSonde.setNumParameter(cmdSonde.getChNames()[0].length);
		dlpSonde.setUnits(cmdSonde.getUnits()[0]);
		dlpSonde.start();

		// Templine
		DataLineProcessor_x dlpTempline = new DataLineProcessor_x(this,
				"Templine");
		CommandList cmdTempline = conf.createTemplineCmdList("Templine");
		dlpTempline.setAddressAndPort(cmdSonde.getRemoteDTAddress()[0]);
		dlpTempline.setChNames(cmdTempline.getChNames()[0]);
		dlpTempline.setDelimiter(cmdTempline.getDelimiter()[0]);
		dlpTempline.setdTypes(cmdTempline.getDTypes()[0]);
		dlpTempline.setMIMEs(cmdTempline.getMIMEs()[0]);
		dlpTempline.setNumParameter(cmdTempline.getChNames()[0].length);
		dlpTempline.setUnits(cmdTempline.getUnits()[0]);
		dlpTempline.start();

		// onBoardTemp
		DataLineProcessor_x dlpOnBoardTemp = new DataLineProcessor_x(this,
				"temperature");
		CommandList cmdTemp = conf.createTempCmdList("temperature");
		dlpOnBoardTemp.setAddressAndPort(cmdTemp.getRemoteDTAddress()[0]);
		dlpOnBoardTemp.setChNames(cmdTemp.getChNames()[0]);
		dlpOnBoardTemp.setDelimiter(cmdTemp.getDelimiter()[0]);
		dlpOnBoardTemp.setdTypes(cmdTemp.getDTypes()[0]);
		dlpOnBoardTemp.setMIMEs(cmdTemp.getMIMEs()[0]);
		dlpOnBoardTemp.setNumParameter(cmdTemp.getChNames()[0].length);
		dlpOnBoardTemp.setUnits(cmdTemp.getUnits()[0]);
		dlpOnBoardTemp.start();

		// onBoardHumi
		DataLineProcessor_x dlpOnBoardHumi = new DataLineProcessor_x(this,
				"humidity");
		CommandList cmdHumi = conf.createHumiCmdList("humidity");
		dlpOnBoardHumi.setAddressAndPort(cmdHumi.getRemoteDTAddress()[0]);
		dlpOnBoardHumi.setChNames(cmdHumi.getChNames()[0]);
		dlpOnBoardHumi.setDelimiter(cmdHumi.getDelimiter()[0]);
		dlpOnBoardHumi.setdTypes(cmdHumi.getDTypes()[0]);
		dlpOnBoardHumi.setMIMEs(cmdHumi.getMIMEs()[0]);
		dlpOnBoardHumi.setNumParameter(cmdHumi.getChNames()[0].length);
		dlpOnBoardHumi.setUnits(cmdHumi.getUnits()[0]);
		dlpOnBoardHumi.start();

		// onBoardVolt
		DataLineProcessor_x dlpOnBoardVolt = new DataLineProcessor_x(this,
				"voltage");
		CommandList cmdVolt = conf.createVoltCmdList("voltage");
		dlpOnBoardVolt.setAddressAndPort(cmdVolt.getRemoteDTAddress()[0]);
		dlpOnBoardVolt.setChNames(cmdVolt.getChNames()[0]);
		dlpOnBoardVolt.setDelimiter(cmdVolt.getDelimiter()[0]);
		dlpOnBoardVolt.setdTypes(cmdVolt.getDTypes()[0]);
		dlpOnBoardVolt.setMIMEs(cmdVolt.getMIMEs()[0]);
		dlpOnBoardVolt.setNumParameter(cmdVolt.getChNames()[0].length);
		dlpOnBoardVolt.setUnits(cmdVolt.getUnits()[0]);
		dlpOnBoardVolt.start();

		// Vaisela Weather Station
		DataLineProcessor_x dlpVWS = new DataLineProcessor_x(this, "VWS");
		CommandList cmdVWS = conf.createVWSCmdList("VWS");
		dlpVWS.setAddressAndPort(cmdVWS.getRemoteDTAddress()[0]);
		dlpVWS.setChNames(cmdVWS.getChNames()[0]);
		dlpVWS.setDelimiter(cmdVWS.getDelimiter()[0]);
		dlpVWS.setdTypes(cmdVWS.getDTypes()[0]);
		dlpVWS.setMIMEs(cmdVWS.getMIMEs()[0]);
		dlpVWS.setNumParameter(cmdVWS.getChNames()[0].length);
		dlpVWS.setUnits(cmdVWS.getUnits()[0]);
		dlpVWS.start();

		dlpList.add(dlpSonde);
		dlpList.add(dlpTempline);
		dlpList.add(dlpOnBoardTemp);
		dlpList.add(dlpOnBoardHumi);
		dlpList.add(dlpOnBoardVolt);
		dlpList.add(dlpVWS);

		return dlpList;
	}

	public void call(String str) {
		Log.i(TAG, "A string was received from the activity. String: " + str);
	}

	private void msgToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
		// log.writelnT(msg);
	}

	// -----------------------------------------------------------------------------

	public void stopService() {
		if (isRunningService) {
			isRunningService = false;
			Log.w(getClass().getName(), "Got to stop()!");
			stopForeground(true);
			msgToast("Stoped service on stop()");
		}
	}

	@Override
	public void onDestroy() {
		stopService();
		Toast.makeText(this, "service done onDestroy", Toast.LENGTH_LONG)
				.show();
		Log.i(TAG, "Service donde onDestroy().");
	}

	/**
	 * Class used for the client Binder. Because we know this service always
	 * runs in the same process as its clients, we don't need to deal with IPC.
	 */
	public class LocalBinder extends Binder {
		DataLineProcessor4RemoteDT_Service getService() {
			// Return this instance of LocalService so clients can call public
			// methods
			return DataLineProcessor4RemoteDT_Service.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

}
