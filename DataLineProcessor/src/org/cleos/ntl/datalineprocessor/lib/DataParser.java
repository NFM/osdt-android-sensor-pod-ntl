/*
 Copyright 2012 University of California, San Diego

 Licensed under the Apache License, Version 1.1 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-1.1

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/**
 * DataParser.java
 * 
 * Receive a data line a parse it
 * 
 * @author Peter Shin, Gesuri Ramirez
 * @date July 2012
 */

package org.cleos.ntl.datalineprocessor.lib;

import java.util.StringTokenizer;

public class DataParser {

	StringTokenizer st;
	String delimiter;

	public DataParser() {
		this.delimiter = ",";
	}

	public DataParser(String del) {
		this.delimiter = del;
	}

	public String[] getStringData(String line, String del) {

		st = new StringTokenizer(line, del);

		String[] data = new String[st.countTokens()];

		for (int i = 0; i < data.length; i++) {
			data[i] = st.nextToken();
		}
		st = null;
		return data;
	}

	public boolean checkDelimiterCount(String dLine, String del, int count) {

		st = new StringTokenizer(dLine, del);

		if (st.countTokens() == count) {
			st = null;
			return true;
		} else {
			st = null;
			return false;
		}
	}

	public boolean checkDoubleTypeData(String data) {
		try {
			Double.parseDouble(data);
		} catch (NumberFormatException ne) {
			ne.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean checkIntTypeData(String data) {
		try {
			Integer.parseInt(data);
		} catch (NumberFormatException ne) {
			ne.printStackTrace();
			return false;
		}
		return true;
	}

}