/*
 Copyright 2012 University of California, San Diego

 Licensed under the Apache License, Version 1.1 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-1.1

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/**
 * Constats.java
 * 
 * @author Gesuri Ramirez, Peter Shin
 * @Date August 2012
 *
 */

package org.cleos.android.lib;

public class Constants {
	/**
	 * Broadcasts
	 * */
	// for the RBNB server 
	public static final String BROADCASTRECEIVER_RESTART_RBNB = "org.cleos.RBNB.broadcastreceiver.RESTART_RBNB";
	public static final String BROADCASTRECEIVER_ERROR_RBNB = "org.cleos.RBNB.broadcastreceiver.ERROR_RBNB";
	public static final String BROADCASTRECEIVER_RBNB_LOCK = "org.cleos.RBNB.broadcastreceiver.LOCK";
	public static final String BROADCASTRECEIVER_RBNB_UNLOCK = "org.cleos.RBNB.broadcastreceiver.UNLOCK";
	
	// for the DataLineProcessor
	public static final String BROADCASTRECEIVER_DLP_RESTART = "org.cleos.ntl.datalineprocessor.broadcastreceiver.RESTART";
	public static final String BROADCASTRECEIVER_DLP_PROCESSDATALINE = "org.cleos.ntl.datalineprocessor.broadcastreceiver.PROCESSDATALINE";
	public static final String BROADCASTRECEIVER_DLP_LOCK = "org.cleos.ntl.datalineprocessor.broadcastreceiver.LOCK";
	public static final String BROADCASTRECEIVER_DLP_UNLOCK = "org.cleos.ntl.datalineprocessor.broadcastreceiver.UNLOCK";
	public static final String BROADCASTRECEIVER_DLP_STOP = "org.cleos.ntl.datalineprocessor.broadcastreceiver.STOP";
	
	// for the DataLineProcessor4RemoteDT
	public static final String BROADCASTRECEIVER_DLP4RDT_RESTART = "org.cleos.ntl.datalineprocessor4remotedt.broadcastreceiver.RESTART";
	public static final String BROADCASTRECEIVER_DLP4RDT_PROCESSDATALINE = "org.cleos.ntl.datalineprocessor4remotedt.broadcastreceiver.PROCESSDATALINE";
	public static final String BROADCASTRECEIVER_DLP4RDT_LOCK = "org.cleos.ntl.datalineprocessor4remotedt.broadcastreceiver.LOCK";
	public static final String BROADCASTRECEIVER_DLP4RDT_UNLOCK = "org.cleos.ntl.datalineprocessor4remotedt.broadcastreceiver.UNLOCK";
	public static final String BROADCASTRECEIVER_DLP4RDT_STOP = "org.cleos.ntl.datalineprocessor4remotedt.broadcastreceiver.STOP";
	
	/**
	 * 
	 */
	public static final String LOCK_RBNB_SERVICE_FLAG_FILE = "lockRBNBService.flag";
	public static final String LOCK_DLP_FLAG_FILE = "lockDLPService.flag";
	public static final String COUNTER_LOCKS_FILE = "counterLocks.integer";
	public static final int LOCK = 7;
	public static final int UNLOCK = 5;
	
	// for the DataLineProcessor
	public static final String SLC_NAME = "SLC_name";
	public static final String DATALINE = "dataLine";
	
	// keyname for intents
	public static final String REMOTE_CALL = "remoteCall"; // for a boolean value
	public static final String CALLED_FROM = "calledFrom"; // for a byte value
	public static final String ACTION = "action"; // for a byte value
	
	// values for intents
		// CALLED_FROM
	public static final byte ACTIVITY = 99;
	public static final byte CONTROLLER = 98; 
	public static final byte BOOT = 97;
	public static final byte SOURCE = 96;
	public static final byte MONITOR = 95;
		// ACTION
	public static final byte STOP = 0; // stop the service
	public static final byte START = 1; // start the service
	public static final byte RESTART = 2; // stop and then start the service
	
	public static String getString(byte value){
		String str = null;
		switch (value){
			case ACTIVITY:
				str = "Activity";
				break;
			case CONTROLLER:
				str = "Controller";
				break;
			case BOOT:
				str = "Boot";
				break;
			case SOURCE:
				str = "Source";
				break;
			case MONITOR:
				str = "Monitor";
				break;
			case STOP:
				str = "Stop";
				break;
			case START:
				str = "Start";
				break;
			case RESTART:
				str = "Restart";
				break;
		}
		return str;
	}
}
